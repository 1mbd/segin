﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using BankDBSegin;
using System.Collections.Generic;

namespace BankDBTestSegin
{
    [TestClass]
    public class UnitTestSegin
    {
        [TestMethod]
        [DataRow(1, 1, 2)]
        [DataRow(2, 2, 4)]
        [DataRow(3, 3, 6)]
        [DataRow(0, 0, 1)] // The test run with this row fails
        public void AddIntegers_FromDataRowTest(int x, int y, int expected)
        {
            var target = new MathSegin();
            int actual = target.AddIntegers(x, y);
            Assert.AreEqual(expected, actual,
                "x:<{0}> y:<{1}>",
                new object[] { x, y });
        }


        public static IEnumerable<object[]> AdditionData
        {
            get
            {
                return new[]
                {
            new object[] { 1, 1, 2 },
            new object[] { 2, 2, 4 },
            new object[] { 3, 3, 6 },
            new object[] { 0, 0, 1 }, // The test run with this row fails
        };
            }
        }

        [TestMethod]
        [DynamicData(nameof(AdditionData))]
        public void AddIntegers_FromDynamicDataTest(int x, int y, int expected)
        {
            var target = new MathSegin();
            int actual = target.AddIntegers(x, y);
            Assert.AreEqual(expected, actual,
                "x:<{0}> y:<{1}>",
                new object[] { x, y });
        }
        public TestContext TestContext { get; set; }
        [TestMethod]
        [DeploymentItem("F:\\BDSegin2.xlsx")]
        [DataSource("MyExcelDataSource")]
        public void AddIntegers_FromDataSourceTest()
        {
            var target = new MathSegin();

            // Access the data
            int x = Convert.ToInt32(TestContext.DataRow["FirstNumber"]);
            int y = Convert.ToInt32(TestContext.DataRow["SecondNumber"]);
            int expected = Convert.ToInt32(TestContext.DataRow["Sum"]);
            int actual = target.AddIntegers(x, y);
            Assert.AreEqual(expected, actual,
                "x:<{0}> y:<{1}>",
                new object[] { x, y });
        }
    }
}
