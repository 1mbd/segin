﻿namespace CodedUITestProject2
{
    using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
    using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;
    using System;
    using System.Collections.Generic;
    using System.CodeDom.Compiler;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    using System.Drawing;
    using System.Windows.Input;
    using System.Text.RegularExpressions;


    public partial class UIMap
    {

        /// <summary>
        /// RecordedMethod1 - Используйте "RecordedMethod1Params" для передачи параметров в этот метод.
        /// </summary>
        public void RecordedMethod1()
        {
            #region Variable Declarations
            WpfButton uIZapuskButton = this.UIMainWindowWindow1.UIZapuskButton;
            WpfCheckBox uIFlagCheckBox = this.UIMainWindowWindow1.UIFlagCheckBox;
            WinButton uIЗакрытьButton = this.UIMainWindowWindow.UIЗакрытьButton;
            #endregion

            // Запуск "%USERPROFILE%\source\repos\BankSegin3\BankSegin3\bin\Debug\net5.0-windows\SimpleWPFAppSegin.exe"
            ApplicationUnderTest uIMainWindowWindow = ApplicationUnderTest.Launch(this.RecordedMethod1Params.UIMainWindowWindowExePath, this.RecordedMethod1Params.UIMainWindowWindowAlternateExePath);

            // Щелкните "Zapusk" кнопка
            Mouse.Click(uIZapuskButton, new Point(16, 8));

            // Выбор "Flag" флажок
            uIFlagCheckBox.Checked = this.RecordedMethod1Params.UIFlagCheckBoxChecked;

            // Щелкните "Закрыть" кнопка
            Mouse.Click(uIЗакрытьButton, new Point(24, 6));
        }

        public virtual RecordedMethod1Params RecordedMethod1Params
        {
            get
            {
                if ((this.mRecordedMethod1Params == null))
                {
                    this.mRecordedMethod1Params = new RecordedMethod1Params();
                }
                return this.mRecordedMethod1Params;
            }
        }

        private RecordedMethod1Params mRecordedMethod1Params;
    }
    /// <summary>
    /// Параметры для передачи в "RecordedMethod1"
    /// </summary>
    [GeneratedCode("Построитель кодированных тестов ИП", "16.0.32802.440")]
    public class RecordedMethod1Params
    {

        #region Fields
        /// <summary>
        /// Запуск "%USERPROFILE%\source\repos\BankSegin3\BankSegin3\bin\Debug\net5.0-windows\SimpleWPFAppSegin.exe"
        /// </summary>
        public string UIMainWindowWindowExePath = "C:\\Users\\segin\\source\\repos\\BankSegin3\\BankSegin3\\bin\\Debug\\net5.0-windows\\Simple" +
            "WPFAppSegin.exe";

        /// <summary>
        /// Запуск "%USERPROFILE%\source\repos\BankSegin3\BankSegin3\bin\Debug\net5.0-windows\SimpleWPFAppSegin.exe"
        /// </summary>
        public string UIMainWindowWindowAlternateExePath = "%USERPROFILE%\\source\\repos\\BankSegin3\\BankSegin3\\bin\\Debug\\net5.0-windows\\SimpleW" +
            "PFAppSegin.exe";

        /// <summary>
        /// Выбор "Flag" флажок
        /// </summary>
        public bool UIFlagCheckBoxChecked = true;
        #endregion
    }
}
